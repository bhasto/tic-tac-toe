package sk.hasto.nn.tictactoe;

import java.util.HashSet;
import java.util.Set;

public class RuleBasedPlayer extends Player {

	private static final Position TOP_LEFT = new Position(0, 0);
	private static final Position TOP_CENTER = new Position(0, 1);
	private static final Position TOP_RIGHT = new Position(0, 2);
	private static final Position CENTER_LEFT = new Position(1, 0);
	private static final Position CENTER = new Position(1, 1);
	private static final Position CENTER_RIGHT = new Position(1, 2);
	private static final Position BOTTOM_LEFT = new Position(2, 0);
	private static final Position BOTTOM_CENTER = new Position(2, 1);
	private static final Position BOTTOM_RIGHT = new Position(2, 2);


	public Position react(Board board) {
		Set<Position> winningPositions = findAllWinningPositionsFor(board, mySymbol);
		if (winningPositions.size() > 0) return firstOf(winningPositions);

		Set<Position> positionsRequiredToBlock = findAllWinningPositionsFor(board, opponentSymbol);
		if (positionsRequiredToBlock.size() > 0) return firstOf(positionsRequiredToBlock);

		Set<Position> forkPositions = findAllAllForkPositionsFor(board, mySymbol);
		if (forkPositions.size() > 0) return firstOf(forkPositions);

		if (board.isFreeOn(CENTER)) return CENTER;

		Set<Position> freeCornerPositions = findAllFreeCornerPositions(board);
		if(freeCornerPositions.size() > 0) return firstOf(freeCornerPositions);

		Set<Position> freeSidePositions = findAllFreeSidePositions(board);
		if (freeSidePositions.size() > 0) return firstOf(freeSidePositions);

		throw new IllegalArgumentException("Cannot make move on given board, it is full.");
	}

	private static <T> T firstOf(Set<T> set) {
		assert set.size() > 0;
		return set.iterator().next();
	}

	private static Set<Position> findAllFreeSidePositions(Board board) {
		Set<Position> positions = new HashSet<Position>();

		if (board.isFreeOn(CENTER_LEFT)) positions.add(CENTER_LEFT);
		if (board.isFreeOn(CENTER_RIGHT)) positions.add(CENTER_RIGHT);
		if (board.isFreeOn(TOP_CENTER)) positions.add(TOP_CENTER);
		if (board.isFreeOn(BOTTOM_CENTER)) positions.add(BOTTOM_CENTER);

		return positions;
	}

	private static Set<Position> findAllFreeCornerPositions(Board board) {
		Set<Position> positions = new HashSet<Position>();

		if (board.isFreeOn(TOP_LEFT)) positions.add(TOP_LEFT);
		if (board.isFreeOn(TOP_RIGHT)) positions.add(TOP_RIGHT);
		if (board.isFreeOn(BOTTOM_LEFT)) positions.add(BOTTOM_LEFT);
		if (board.isFreeOn(BOTTOM_RIGHT)) positions.add(BOTTOM_RIGHT);

		return positions;
	}

	private static Set<Position> findAllAllForkPositionsFor(Board board, Symbol symbol) {
		Set<Position> forkPositions = new HashSet<Position>();

		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				if (board.isFreeOn(row, col)) {
					Board boardAfterPossibleMove = board.copy();
					boardAfterPossibleMove.put(symbol, row, col);
					Set<Position> winningPositionsAfterMove = findAllWinningPositionsFor(board, symbol);
					if (winningPositionsAfterMove.size() > 1) forkPositions.add(new Position(row, col));
				}
			}
		}

		return forkPositions;
	}

	private static Set<Position> findAllWinningPositionsFor(Board board, Symbol symbol) {
		Set<Position> positions = new HashSet<Position>();

		for (Line line : board.lines()) {
			if (line.canBeFilledBy(symbol)) positions.add(line.findPositionThatFillsLine());
		}

		return positions;
	}
}
