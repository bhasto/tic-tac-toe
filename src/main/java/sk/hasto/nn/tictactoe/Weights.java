package sk.hasto.nn.tictactoe;

public final class Weights {

	public final double[][] inputToHiddenWeights;
	public final double[] hiddenThresholds;
	public final double[] hiddenToOutputWeights;
	public double outputThreshold;

	public Weights(double[][] inputToHiddenWeights, double[] hiddenThresholds, double[] hiddenToOutputWeights, double outputThreshold) {
		this.inputToHiddenWeights = inputToHiddenWeights;
		this.hiddenThresholds = hiddenThresholds;
		this.hiddenToOutputWeights = hiddenToOutputWeights;
		this.outputThreshold = outputThreshold;
	}

	public void multiply(double scalar) {
		for (int i = 0; i < inputToHiddenWeights.length; i++) {
			for (int h = 0; h < inputToHiddenWeights[i].length; h++) {
				inputToHiddenWeights[i][h] *= scalar;
			}
		}

		for (int i = 0; i < hiddenThresholds.length; i++) {
			hiddenThresholds[i] *= scalar;
		}

		for (int i = 0; i < hiddenToOutputWeights.length; i++) {
			hiddenToOutputWeights[i] *= scalar;
		}

		outputThreshold *= scalar;
	}

	public void add(Weights other) {
		for (int i = 0; i < inputToHiddenWeights.length; i++) {
			for (int h = 0; h < inputToHiddenWeights[i].length; h++) {
				inputToHiddenWeights[i][h] += other.inputToHiddenWeights[i][h];
			}
		}

		for (int i = 0; i < hiddenThresholds.length; i++) {
			hiddenThresholds[i] += other.hiddenThresholds[i];
		}

		for (int i = 0; i < hiddenToOutputWeights.length; i++) {
			hiddenToOutputWeights[i] += other.hiddenToOutputWeights[i];
		}

		outputThreshold += other.outputThreshold;
	}
}
