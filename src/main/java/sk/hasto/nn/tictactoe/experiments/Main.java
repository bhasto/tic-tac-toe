package sk.hasto.nn.tictactoe.experiments;

import sk.hasto.nn.tictactoe.Player;
import sk.hasto.nn.tictactoe.RandomPlayer;
import sk.hasto.nn.tictactoe.RuleBasedPlayer;

public class Main {

	public static void againstRuleBasedPlayerInRandomOrder() {
		System.out.println("Teaching against rule based player, players go in random order");
		Player opponent = new RuleBasedPlayer();
		Teacher teacher = new TeacherAgainstGivenOpponent(0.3, 0.1, opponent);
		teacher.teach(100000);
		teacher.report();
		System.out.println();
	}

	public static void againstRuleBasedPlayerOurAgentIsAlwaysX() {
		System.out.println("Teaching against rule based player, our player always X");
		Player opponent = new RuleBasedPlayer();
		Teacher teacher = new TeacherForPositionX(0.3, 0.1, opponent);
		teacher.teach(100000);
		teacher.report();
		System.out.println();
	}

	public static void againstRuleBasedPlayerOurAgentIsAlwaysO() {
		System.out.println("Teaching against rule based player, our player always O");
		Player opponent = new RuleBasedPlayer();
		Teacher teacher = new TeacherForPositionO(0.3, 0.1, opponent);
		teacher.teach(100000);
		teacher.report();
		System.out.println();
	}

	public static void againstRandomPlayer() {
		System.out.println("Teaching against random player");
		Player opponent = new RandomPlayer();
		Teacher teacher = new TeacherAgainstGivenOpponent(0.3, 0.1, opponent);
		teacher.teach(100000);
		teacher.report();
		System.out.println();
	}

	public static void againstAdaptingOpponent() {
		System.out.println("Teaching against adapting opponent (another NN)");
		Teacher teacher = new TeacherAgainstAdaptingOpponent(0.3, 0.1);
		teacher.teach(100000);
		teacher.report();
		System.out.println();
	}

	public static void main(String[] args) {
		againstRuleBasedPlayerInRandomOrder();
		againstRuleBasedPlayerOurAgentIsAlwaysX();
		againstRuleBasedPlayerOurAgentIsAlwaysO();
		againstRandomPlayer();
		againstAdaptingOpponent();
	}

}
