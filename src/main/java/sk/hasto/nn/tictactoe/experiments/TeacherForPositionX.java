package sk.hasto.nn.tictactoe.experiments;

import sk.hasto.nn.tictactoe.Game;
import sk.hasto.nn.tictactoe.NeuralNetworkPlayer;
import sk.hasto.nn.tictactoe.Player;
import sk.hasto.nn.tictactoe.Symbol;

public class TeacherForPositionX extends Teacher {

	private final double temporalFactor;
	private final double learningSpeed;
	private final Player opponent;

	public TeacherForPositionX(double temporalFactor, double learningSpeed, Player opponent) {
		this.temporalFactor = temporalFactor;
		this.learningSpeed = learningSpeed;
		this.opponent = opponent;
	}

	private NeuralNetworkPlayer student;
	private boolean studentIsCurrentlyX;

	@Override
	public void teach(int epochs) {
		student = new NeuralNetworkPlayer(temporalFactor, learningSpeed);

		for (int epoch = 0; epoch < epochs; epoch++) {
			studentIsCurrentlyX = true;
			Game game = new Game(student, opponent, this);
			game.run();
			recordEpoch();
		}
	}

	@Override
	protected Symbol studentsCurrentSymbol() {
		return studentIsCurrentlyX ? Symbol.X : Symbol.O;
	}

	@Override
	public void tie() {
		super.tie();
		student.reward(0.5);
	}

	@Override
	public void winnerWas(Symbol winningSymbol) {
		super.winnerWas(winningSymbol);

		if (winningSymbol == studentsCurrentSymbol()) {
			student.reward(1);
		} else {
			student.reward(0);
		}
	}
}
