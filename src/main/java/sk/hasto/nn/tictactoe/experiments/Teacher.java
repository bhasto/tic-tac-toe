package sk.hasto.nn.tictactoe.experiments;

import java.util.List;

import com.google.common.collect.Lists;

import sk.hasto.nn.tictactoe.Symbol;

public abstract class Teacher {

	private static final int REPORTING_CHUNK_SIZE = 100;

	private int totalEpochs = 0;
	private int currentChunkSize = 0;
	private int winsInCurrentChunk = 0;
	private int losesInCurrentChunk = 0;
	private int tiesInCurrentChunk = 0;

	private final List<Integer> wins = Lists.newArrayListWithCapacity(1000);
	private final List<Integer> loses = Lists.newArrayListWithCapacity(1000);
	private final List<Integer> ties = Lists.newArrayListWithCapacity(1000);

	protected abstract Symbol studentsCurrentSymbol();

	public abstract void teach(int epochs);

	protected final void recordEpoch() {
		totalEpochs++;

		// make stats in 100 epoch chunks
		currentChunkSize++;
		if (currentChunkSize % 100 == 0) {
			wins.add(winsInCurrentChunk);
			ties.add(tiesInCurrentChunk);
			loses.add(losesInCurrentChunk);
			clearCurrentChunk();
		}
	}

	public void report() {
		System.out.println("Total epochs: " + totalEpochs);
		System.out.println("Results (in " + REPORTING_CHUNK_SIZE + " epoch chunks)");
		System.out.println("Wins: " + formatList(wins));
		System.out.println("Ties: " + formatList(ties));
		System.out.println("Loses: " + formatList(loses));
	}

	private String formatList(List<Integer> values) {
		StringBuilder builder = new StringBuilder(5000);

		for (Integer value : values) {
			builder.append(value);
			builder.append(';');
		}

		return builder.toString();
	}

	private void clearCurrentChunk() {
		winsInCurrentChunk = 0;
		losesInCurrentChunk = 0;
		tiesInCurrentChunk = 0;
		currentChunkSize = 0;
	}

	public void tie() {
		tiesInCurrentChunk++;
	}

	public void winnerWas(Symbol symbol) {
		if (symbol == studentsCurrentSymbol()) winsInCurrentChunk++;
		else losesInCurrentChunk++;
	}
}
