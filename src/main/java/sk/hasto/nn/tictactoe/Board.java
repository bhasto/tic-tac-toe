package sk.hasto.nn.tictactoe;

import java.util.Arrays;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * Board rows are numbered from top to bottom, starting with 0.
 * Board columns are numbered from left to right starting with 0.
 */
public class Board {

	public static final Symbol FREE = null;

	public final Symbol[][] state;

	public Board() {
		state = new Symbol[][]{
				{FREE, FREE, FREE},
				{FREE, FREE, FREE},
				{FREE, FREE, FREE}
		};
	}

	private Board(Symbol[][] state) {
		this.state = state;
	}

	public Board copy() {
		Symbol[][] stateCopy = {
				Arrays.copyOf(state[0], 3),
				Arrays.copyOf(state[1], 3),
				Arrays.copyOf(state[2], 3)
		};

		return new Board(stateCopy);
	}

	public Symbol get(int row, int col) {
		return state[row][col];
	}

	public void put(Symbol symbol, int row, int col) {
		state[row][col] = symbol;
	}

	public boolean isFreeOn(int row, int col) {
		return state[row][col] == FREE;
	}

	public boolean isFreeOn(Position position) {
		return isFreeOn(position.row, position.col);
	}

	public boolean isFull() {
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				Symbol symbol = state[row][col];
				if (symbol == FREE) return false;
			}
		}

		return true;
	}

	private Line getRow(int rowIndex) {
		return new Line(pos(rowIndex, 0), state[rowIndex][0], pos(rowIndex, 1), state[rowIndex][1], pos(rowIndex, 2), state[rowIndex][2]);
	}

	private Line getCol(int colIndex) {
		return new Line(pos(0, colIndex), state[0][colIndex], pos(1, colIndex), state[1][colIndex], pos(2, colIndex), state[2][colIndex]);
	}

	private Line getFirstDiagonal() {
		return new Line(pos(0, 0), state[0][0], pos(1, 1), state[1][1], pos(2, 2), state[2][2]);
	}

	private Line getSecondDiagonal() {
		return new Line(pos(0, 2), state[0][2], pos(1, 1), state[1][1], pos(2, 0), state[2][0]);
	}

	private static Position pos(int row, int col) {
		return new Position(row, col);
	}

	public Iterable<Line> lines() {
		Set<Line> lines = Sets.newHashSetWithExpectedSize(8);

		for (int i = 0; i < 3; i++) {
			lines.add(getRow(i));
			lines.add(getCol(i));
		}

		lines.add(getFirstDiagonal());
		lines.add(getSecondDiagonal());

		return lines;
	}
}
