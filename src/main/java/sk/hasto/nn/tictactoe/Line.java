package sk.hasto.nn.tictactoe;

public final class Line {

	private final Position[] positions = new Position[3];
	private final Symbol[] symbols = new Symbol[3];

	public Line(Position pos0, Symbol symbol0, Position pos1, Symbol symbol1, Position pos2, Symbol symbol2) {
		positions[0] = pos0;
		positions[1] = pos1;
		positions[2] = pos2;

		symbols[0] = symbol0;
		symbols[1] = symbol1;
		symbols[2] = symbol2;
	}

	public boolean canBeFilledBy(Symbol symbol) {
		int symbolCount = countSymbol(symbol);
		int freeSpaceCount = countSymbol(Board.FREE);
		return (symbolCount == 2) && (freeSpaceCount == 1);
	}

	public Position findPositionThatFillsLine() {
		for (int i = 0; i < 3; i++) {
			if (symbols[i] == Board.FREE) return positions[i];
		}

		throw new IllegalStateException("Line has no free positions.");
	}

	public boolean isFilledByOneSymbol() {
		return (symbols[0] != Board.FREE) && (symbols[0] == symbols[1]) && (symbols[1] == symbols[2]);
	}

	public Symbol symbolThatFillsLine() {
		assert isFilledByOneSymbol();
		return symbols[0];
	}

	private int countSymbol(Symbol symbolToCount) {
		int count = 0;
		for (Symbol symbol : symbols) {
			if (symbol == symbolToCount) count++;
		}

		return count;
	}
}
