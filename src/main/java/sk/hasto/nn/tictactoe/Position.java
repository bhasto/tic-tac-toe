package sk.hasto.nn.tictactoe;

public final class Position implements Comparable<Position> {

	public final int row;
	public final int col;

	public Position(int row, int col) {
		this.row = row;
		this.col = col;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Position position = (Position) o;
		return (row == position.row) && (col == position.col);
	}

	@Override
	public int hashCode() {
		int result = row;
		result = 31 * result + col;
		return result;
	}

	@Override
	public int compareTo(Position other) {
		if (row == other.row) {
			return col - other.col;
		}

		return row - other.row;
	}
}
