package sk.hasto.nn.tictactoe;

import sk.hasto.nn.tictactoe.experiments.Teacher;

public class Game {

	private static final Symbol NO_WINNING_SYMBOL = null;

	private final Player playerX;
	private final Player playerO;
	private final Board board;
	private final Teacher teacher;

	private Player playerOnTurn;


	public Game(Player playerX, Player playerO, Teacher teacher) {
		this.board = new Board();
		this.teacher = teacher;

		this.playerX = playerX;
		playerX.assignSymbol(Symbol.X);

		this.playerO = playerO;
		playerO.assignSymbol(Symbol.O);
	}

	public void run() {
		playerOnTurn = playerX;

		while (!isOver()) {
			Position move = playerOnTurn.react(board);
			board.put(playerOnTurn.mySymbol, move.row, move.col);
			switchPlayers();
		}

		notifyPlayersAboutResult();
	}

	private void notifyPlayersAboutResult() {
		Symbol winningSymbol = findWinningSymbol();

		if (winningSymbol == NO_WINNING_SYMBOL) {
			teacher.tie();
		} else {
			teacher.winnerWas(winningSymbol);
		}
	}

	private void switchPlayers() {
		Player newPlayerOnTurn = (playerOnTurn == playerX) ? playerO : playerX;
		playerOnTurn = newPlayerOnTurn;
	}

	public boolean isOver() {
		if (board.isFull()) return true;

		for (Line line : board.lines()) {
			if (line.isFilledByOneSymbol()) return true;
		}

		return false;
	}

	public Symbol findWinningSymbol() {
		for (Line line : board.lines()) {
			if (line.isFilledByOneSymbol()) return line.symbolThatFillsLine();
		}

		return NO_WINNING_SYMBOL;
	}
}
