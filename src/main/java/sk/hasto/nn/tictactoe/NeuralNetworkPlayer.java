package sk.hasto.nn.tictactoe;

import java.util.List;
import java.util.Random;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.mathutil.randomize.RangeRandomizer;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;

public class NeuralNetworkPlayer extends Player {

	private static final Random RANDOM = new Random();

	private static final int INPUT_NEURON_COUNT = 9;
	private static final int HIDDEN_NEURON_COUNT = 27;
	private final double alpha;
	private final double lambda;

	private final BasicNetwork network = new BasicNetwork();

	private final List<double[]> history = Lists.newArrayListWithCapacity(5);

	public NeuralNetworkPlayer(double temporalFactor, double learningSpeed) {
		this.alpha = learningSpeed;
		this.lambda = temporalFactor;
		network.addLayer(new BasicLayer(null, true, INPUT_NEURON_COUNT));
		network.addLayer(new BasicLayer(new ActivationSigmoid(), true, HIDDEN_NEURON_COUNT));
		network.addLayer(new BasicLayer(new ActivationSigmoid(), false, 1));
		network.getStructure().finalizeStructure();
		new RangeRandomizer(-2, 2).randomize(network);
	}

	public Position react(Board board) {
		Set<Position> possibleMoves = findPossibleMoves(board);

		Position bestMove = null;
		double bestEvaluation = 0;
		for (Position move : possibleMoves) {
			double[] nnInput = codePossibleMoveForNeuralNetwork(board, move);
			double nnOutput = computeOutput(nnInput);
			if (nnOutput > bestEvaluation) {
				bestEvaluation = nnOutput;
				bestMove = move;
			}
		}

		// pseudo-randomly determine next move
		Position chosenMove;
		double p = Math.random();
		if (p <= bestEvaluation) {
			chosenMove = bestMove;
		} else {
			int randomPositionIndex = RANDOM.nextInt(possibleMoves.size());
			chosenMove = Lists.newArrayList(possibleMoves).get(randomPositionIndex);
		}

		double[] chosenMoveEncoded = codePossibleMoveForNeuralNetwork(board, chosenMove);
		history.add(chosenMoveEncoded);
		return chosenMove;
	}

	private double computeOutput(double[] input) {
		double[] output = new double[1];
		network.compute(input, output);
		return output[0];
	}

	private double[] codePossibleMoveForNeuralNetwork(Board board, Position move) {
		Board boardAfterMove = board.copy();
		boardAfterMove.put(mySymbol, move.row, move.col);

		double[] coded = new double[9];
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				int index = (3 * row) + col;
				coded[index] = codeSymbolForNeuralNetwork(boardAfterMove.get(row, col));
			}
		}

		return coded;
	}

	private int codeSymbolForNeuralNetwork(Symbol symbol) {
		if (symbol == Board.FREE) return 0;
		return (symbol == mySymbol) ? 1 : -1;
	}

	private Set<Position> findPossibleMoves(Board board) {
		Set<Position> possibleMoves = Sets.newHashSetWithExpectedSize(9);

		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				if (board.isFreeOn(row, col)) {
					possibleMoves.add(new Position(row, col));
				}
			}
		}

		return possibleMoves;
	}

	public void reward(double reward) {
		double outputAfterStepTPlus1 = reward;

		Weights deltaSum = null;
		for (int t = history.size() - 1; t >= 0; t--) {
			double[] boardAfterStepT = history.get(t);
			double outputAfterStepT = computeOutput(boardAfterStepT);

			double coefficient = alpha * (outputAfterStepTPlus1 - outputAfterStepT);
			Weights deltas = eFunction(t, lambda);
			deltas.multiply(coefficient);
			if (deltaSum == null) {
				deltaSum = deltas;
			} else {
				deltaSum.add(deltas);
			}

			outputAfterStepTPlus1 = outputAfterStepT;
		}

		updateWeights(deltaSum);
		history.clear();
	}

	private void updateWeights(Weights weights) {
		for (int i = 0; i < weights.inputToHiddenWeights.length; i++) {
			for (int h = 0; h < weights.inputToHiddenWeights[i].length; h++) {
				double delta = weights.inputToHiddenWeights[i][h];
				network.addWeight(0, i, h, delta);
			}
		}

		for (int i = 0; i < weights.hiddenThresholds.length; i++) {
			double delta = weights.hiddenThresholds[i];
			network.addWeight(0, INPUT_NEURON_COUNT, i, delta);
		}

		for (int i = 0; i < weights.hiddenToOutputWeights.length; i++) {
			double delta = weights.hiddenToOutputWeights[i];
			network.addWeight(1, i, 0, delta);
		}

		network.addWeight(1, HIDDEN_NEURON_COUNT, 0, weights.outputThreshold);
	}

	private Weights eFunction(int t, double lambda) {
		Weights result;
		if (t == 0) {
			result = computeGradients(0); // gradP0
		} else {
			// lambda * e_{t-1} * gradPt
			result = eFunction(t - 1, lambda);
			result.multiply(lambda);
			result.add(computeGradients(t));
		}

		return result;
	}

	private Weights computeGradients(int t) {
		double[] input = history.get(t);
		double networkOutput = computeOutput(input);
		double outputThreshold = networkOutput * (1 - networkOutput); // just one output neuron

		double[] hiddenToOutputWeights = new double[HIDDEN_NEURON_COUNT]; // for each synapse going from hidden layer to output neuron
		for (int i = 0; i < HIDDEN_NEURON_COUNT; i++) {
			double hiddenNeuronOutput = network.getLayerOutput(1, i);
			hiddenToOutputWeights[i] = hiddenNeuronOutput * outputThreshold;
		}

		double[] hiddenThreshold = new double[HIDDEN_NEURON_COUNT]; // for each hidden neuron
		for (int i = 0; i < HIDDEN_NEURON_COUNT; i++) {
			double hiddenNeuronOutput = network.getLayerOutput(1, i);
			double outputNeuronWeight = network.getWeight(1, i, 0);
			hiddenThreshold[i] = hiddenNeuronOutput * (1 - hiddenNeuronOutput) * outputThreshold * outputNeuronWeight;
		}

		double[][] inputToHiddenWeights = new double[INPUT_NEURON_COUNT][HIDDEN_NEURON_COUNT]; // for each synapse going from input layer to hidden layer
		for (int i = 0; i < INPUT_NEURON_COUNT; i++) {
			for (int h = 0; h < HIDDEN_NEURON_COUNT; h++) {
				inputToHiddenWeights[i][h] = hiddenThreshold[h] * input[i];
			}
		}

		return new Weights(inputToHiddenWeights, hiddenThreshold, hiddenToOutputWeights, outputThreshold);
	}
}
