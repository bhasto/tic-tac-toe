package sk.hasto.nn.tictactoe;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomPlayer extends Player {

	private static final Position TOP_LEFT = new Position(0, 0);
	private static final Position TOP_CENTER = new Position(0, 1);
	private static final Position TOP_RIGHT = new Position(0, 2);
	private static final Position CENTER_LEFT = new Position(1, 0);
	private static final Position CENTER = new Position(1, 1);
	private static final Position CENTER_RIGHT = new Position(1, 2);
	private static final Position BOTTOM_LEFT = new Position(2, 0);
	private static final Position BOTTOM_CENTER = new Position(2, 1);
	private static final Position BOTTOM_RIGHT = new Position(2, 2);

	private static final Random RANDOM = new Random();

	public Position react(Board board) {
		List<Position> freePositions = findAllFreePositions(board);
		int randomIndex = RANDOM.nextInt(freePositions.size());
		return freePositions.get(randomIndex);
	}

	private static List<Position> findAllFreePositions(Board board) {
		List<Position> positions = new ArrayList<Position>();

		if (board.isFreeOn(CENTER)) positions.add(CENTER);
		if (board.isFreeOn(TOP_LEFT)) positions.add(TOP_LEFT);
		if (board.isFreeOn(TOP_RIGHT)) positions.add(TOP_RIGHT);
		if (board.isFreeOn(BOTTOM_LEFT)) positions.add(BOTTOM_LEFT);
		if (board.isFreeOn(BOTTOM_RIGHT)) positions.add(BOTTOM_RIGHT);
		if (board.isFreeOn(CENTER_LEFT)) positions.add(CENTER_LEFT);
		if (board.isFreeOn(CENTER_RIGHT)) positions.add(CENTER_RIGHT);
		if (board.isFreeOn(TOP_CENTER)) positions.add(TOP_CENTER);
		if (board.isFreeOn(BOTTOM_CENTER)) positions.add(BOTTOM_CENTER);

		return positions;
	}
}
