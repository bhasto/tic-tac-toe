package sk.hasto.nn.tictactoe;

public abstract class Player {

	protected Symbol mySymbol;
	protected Symbol opponentSymbol;

	public void assignSymbol(Symbol symbol) {
		this.mySymbol = symbol;
		this.opponentSymbol = opponentSymbolTo(symbol);
	}

	private static Symbol opponentSymbolTo(Symbol symbol) {
		return (symbol == Symbol.X) ? Symbol.O : Symbol.X;
	}

	abstract Position react(Board board);
}
